import 'package:flutter/material.dart';
import 'package:quiz_app_langsteiner/question.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Quiz App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage()
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Question> questions = getQuestions();
  int current = 0;
  int score = 0;




  @override
  Widget build(BuildContext context) {
    print("done");

    return Scaffold(
      appBar: AppBar(
        title: Text("Quiz"),
      ),
      body: Center(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,

          children: [

            _questionDisplay(),
            _answerListOrResult()




          ],
        ),
      )
    );
  }

  _questionDisplay() {
    if (current == questions.length) {
      return Column(
        children: [
          Text("Done!", style: TextStyle(fontSize: 24),)
        ],
      );
    }

    return Column(
      children: [
        Text("Question ${current + 1} of ${questions.length}", style: TextStyle(fontSize: 30)),
        Text("Frage: ${questions[current].qText}"),
      ],
    );
  }


  _answerListOrResult() {
    if (current == questions.length) {
      return Column(
        children: [
          Text("You got ${score} points out of ${questions.length}!"),
          _restartButton()
        ],
      );
    }

    return Column(
      children: questions[current]
          .answers
          .map(
            (x) => _answerButton(x),
          ).toList(),
    );
  }


  Widget _answerButton(Answer answer) {
    return Container(
      width: 300,
      margin: const EdgeInsets.symmetric(vertical: 4),
      child: ElevatedButton(
        child: Text(answer.aText),
        onPressed: () {
          if (answer.correct) {
            // correct
            score++;
          }

          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Center(child: Text("${answer.correct}")),
            ),
          );
          //skip to next question
          if (current == questions.length) {
            //done
            //show results
            print(score);
          } else {
            setState(() {
              current++;
            });
          }
        },
      ),
    );
  }


  Widget _restartButton() {
    return Container(
      width: 300,
      margin: const EdgeInsets.symmetric(vertical: 4),
      child: ElevatedButton(
        child: Text("Restart"),
        onPressed: () {
          setState(() {
            current = 0;
            score = 0;
          });
        },
      ),



    );
  }



}
