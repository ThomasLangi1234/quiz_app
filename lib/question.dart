class Question {
  final String qText;
  final List<Answer> answers;

  Question(this.qText, this.answers);


  Answer findCorrect() {
    for (int i = 0; i < answers.length; i++) {
      if (answers.elementAt(i).correct) {
        return answers.elementAt(i);
      }
    }

    throw Error();
  }
}


class Answer {
  final String aText;
  final bool correct;

  Answer(this.aText, this.correct);
}


// not tested and useless:
/*
List<Question> selectQuestions(int i) {
  List<Question> all = getQuestions();
  List<Question> result = <Question>[];
  List<int> ls = <int>[];

  Random random = Random();
  int x = 0;

  while (x < i) {
    int y = random.nextInt(all.length);

    if (ls.contains(y)) {
      continue;
    }
    ls.add(y);

    x++;
  }

  //return result;

  return getQuestions();
}
*/







List<Question> getQuestions() {
  List<Question> result = <Question>[];

  result.add(Question("Was ist das beste Spiel der Welt?",
      [Answer("Minecraft (Pixelhaufen)", true),
      Answer("CSGO", false),
      Answer("Fifa", false),
      Answer("Fortnite", false)]));

  result.add(Question("Wer ist der beste POS Lehrer?",
      [Answer("REFR", false),
        Answer("GITS", false),
        Answer("SCRE", false),
        Answer("MAUS", false),
        Answer("Alle der obigen", true)]));

  result.add(Question("Wer ist am coolsten?",
      [Answer("Chaosflo44", true),
        Answer("Simex", false),
        Answer("Nikocado Avocado", false),
        Answer("HicksMarvin", false)]));

  result.add(Question("Ist diese Uebung ein + Wert?",
      [Answer("Ja, selbstverstaendlich!", true),
        Answer("Nein", false),
        Answer("Vielleicht", false),
        Answer("Vermutlich", false)]));

  result.add(Question("Wer macht die beste Musik?",
      [Answer("Talor Swift (\"meine Tay Tay\" - Kilian Baier)", true),
        Answer("Justin Bieber", false),
        Answer("Ariana Grande", false),
        Answer("Ed Sheeran", false)]));













  return result;
}